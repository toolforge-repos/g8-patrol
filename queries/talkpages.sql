USE enwiki_p;
SELECT tp.page_id, tp.page_title FROM page AS tp
WHERE tp.page_namespace = 1 # Is talk page?
AND tp.page_title NOT LIKE "%/%"
AND NOT EXISTS (SELECT 1 FROM page AS ap WHERE ap.page_namespace = 0 AND ap.page_title = tp.page_title) # Is orphan?
AND NOT EXISTS (SELECT 1 FROM templatelinks WHERE tl_from = tp.page_id AND tl_title = "G8-exempt" AND tl_namespace = 10); # Is not tagged with g8-exempt?